﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Places.Controllers
{
    [ApiController]
    [Route("api/places")]
    public class PlacesController : ControllerBase
    {
        // GET api/places
        [HttpGet]
        public ActionResult GetOrders()
        {
            Place p1 = new Place(1, "Megacenter", "Irpavi");
            Place p2 = new Place(2, "Multicine", "Arce");
            return Ok(new List<Place> { p1, p2 });
        }
        public class Place
        {
            public int id { get; set; }
            public string name { get; set; }
            public string location { get; set; }

            public Place(int id, string name, string location)
            {
                this.id = id;
                this.name = name;
                this.location = location;
            }
        }
    }
}
