﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Events.API.Objects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Events.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventsController : ControllerBase
    {
        

        private readonly ILogger<EventsController> _logger;

        public EventsController(ILogger<EventsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Objects.Events[] Get()
        {
            Objects.Events[] events = new Objects.Events[2];
            
            events[0]=new Objects.Events
            {
                ID = 1,
                Name = "Lolla",
                Capacity = 10000

            };
            events[1] = new Objects.Events
            {
                ID = 2,
                Name = "Coachella",
                Capacity = 30000

            };
            return events;
           
        }
    }
}
