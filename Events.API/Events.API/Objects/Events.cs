﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Events.API.Objects
{
    public class Events
    {
        public int ID { get; set; }
        public String Name{ get; set; }
        public int Capacity { get; set; }

    }
}
